#!/bin/bash

# Configure static.ini

    /bin/sed -i -E "s/logging.directory.*/logging.directory\ =\ \"\/var\/log\/medialib\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/logging.stdout.*/logging.stdout\ =\ \"false\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/logging.level.*/logging.level\ =\ \"$LOGGING_LEVEL\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/db0.host.*/db0.host\ =\ \"$DATABASE_HOST\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/db0.user.*/db0.user\ =\ \"$DATABASE_USER\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/db0.password.*/db0.password\ =\ \"$DATABASE_PASSWORD\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/db0.dbname.*/db0.dbname\ =\ \"$DATABASE_NAME\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/baseUrl.*/baseUrl\ =\ \"$BASE_URL\"/" "/var/www/html/static.ini"
    /bin/sed -i -E "s/debug\ =\ .*/debug\ =\ \"$DEBUG\"/" "/var/www/html/static.ini"

# run server
/usr/sbin/apache2ctl -D FOREGROUND
