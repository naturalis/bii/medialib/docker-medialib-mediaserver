FROM php:7.4-apache-buster
MAINTAINER hugo.vanduijn@naturalis.nl
ARG PHP_MAX_EXECUTION_TIME=60
ARG PHP_MAX_INPUT_TIME=120
ARG PHP_POST_MAX_SIZE=32M
ARG PHP_UPLOAD_MAX_FILESIZE=32M
ARG PHP_MEMORY_LIMIT=512M

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y upgrade \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install apt-utils \
        libcurl4-openssl-dev \
        libpq-dev \
        libpng-dev \
        libjpeg-dev \
        libfreetype6-dev \
        libapache2-mod-xsendfile \
        mariadb-client \
        git \
    && rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install \
    gd \
    pdo_mysql

RUN a2enmod xsendfile \
    && a2enmod rewrite

# Download mediaserver code
RUN git clone https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver.git /var/www/html \
    && cp /var/www/html/static.ini.tpl /var/www/html/static.ini \
    && mkdir /var/log/medialib \
    && chown www-data:www-data /var/log/medialib

# Use the default production configuration for php
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN /bin/sed -i -E "s/max_execution_time = .*/max_execution_time = $PHP_MAX_EXECUTION_TIME/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/max_input_time = .*/max_input_time = $PHP_MAX_INPUT_TIME/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/post_max_size = .*/post_max_size = $PHP_POST_MAX_SIZE/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/upload_max_filesize = .*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/memory_limit = .*/memory_limit = $PHP_MEMORY_LIMIT/" "$PHP_INI_DIR/php.ini"

ADD ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# Serve the application using entrypoint so persistent config is possible
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

EXPOSE 80/tcp